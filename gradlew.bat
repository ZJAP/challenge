<resources>
    <string name="app_name">NoCirculaForms</string>


    <string name="btnSAVE">GUARDAR</string>

    <!--    Formulario Inicio-->

    <string name="titleInicio">INICIO</string>
    <string name="btnRegistrarse">Registrarse</string>
    <string name="btnIniciarSesión">Iniciar Sesión</string>


    <!--    Formulario Registro-->

    <string name="txtUser">Usuario</string>
    <string name="txtEmail">Email</string>
    <string name="txtPassword">Password</string>

    <!--    Formulario Inicio Sesion-->

    <!--    Formulario Ventana Principal-->

    <string name="titleAutosReg">Autos Registrados</string>
    <string name="txtplaca">Placa</string>
    <string name="txtDiaNC">Día sin Circulación</string>
    <string name="EditarAuto">EDITAR</string>
    <string name="btnAnadirAuto">AÑADIR AUTO</string>
    <string name="NoAutosRegistrados">NO TIENE AUTOS CONFIGURADOS</string>


    <!--    Formulario Nuevo Auto-->

    <string name="NuevoAuto">NUEVO AUTO</string>
    <string name="NombreAuto">Identificativo del Auto</string>
    <string name="color">Color</string>
    <string name="fechamat">Fecha Matriculacion</string>
    <string name="btnConfAlarm">Configurar Alarma</string>
    <string name="avisoBluet">Aviso por Deteccion de Dispositivo Bluetooth </string>
    <string name="ConfigurarNuevo">Configurar Nuevo</string>
    <string name="nombreDispositivo">Nombre Dispositivo</string>
    <string name="mac">MAC</string>

    <!--    Formulario Radio Bluetooth-->

    <string name="ConfigBluetoth">Configuracion del Dispositivo Bluetooth</string>
    <string name="DispositivosDisponibles">Dispositivos Disponibles</string>
    <string name="SeleccioneRadio">Seleccione su Radio</string>
    <string name="title_activity_login">Sign in</string>
    <string name="prompt_email">Email</string>
    <string name="prompt_password">Password</string>
    <string name="action_sign_in">Sign in or register</string>
    <string name="action_sign_in_short">Sign in</string>
    <string name="welcome">"Welcome !"</string>
    <string name="invalid_username">Not a valid username</string>
    <string name="invalid_password">Password must be >5 characters</string>
    <string name="login_failed">"Login failed"</string>


</re