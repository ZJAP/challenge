package com.example.challenge_sloncorp.Adapters;


import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.challenge_sloncorp.R;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder>{

    public void setDevices(ArrayList<BluetoothDevice> devices) {
        this.devices = devices;
    }

    private ArrayList<BluetoothDevice> devices =  new ArrayList<>();

    public RVAdapter(ArrayList<BluetoothDevice> devices) {
        this.devices = devices;
    }




    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_items,parent,false);
        RecyclerView.ViewHolder holder = new ViewHolder(view);
        return (ViewHolder) holder;


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.nombre.setText(devices.get(position).getName());
        holder.UID.setText(devices.get(position).getAddress());

        /*if(devices != null){
            holder.nombre.setText(devices.get(position).getName());
            holder.UID.setText(devices.get(position).getAddress());
        }
*/
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView nombre;
        TextView UID;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre);
            UID= itemView.findViewById(R.id.UID);
        }
    }

}
