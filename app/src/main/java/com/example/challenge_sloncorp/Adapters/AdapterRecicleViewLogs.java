package com.example.challenge_sloncorp.Adapters;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.challenge_sloncorp.Classes.Registry;
import com.example.challenge_sloncorp.R;

import java.util.ArrayList;

public class AdapterRecicleViewLogs extends  RecyclerView.Adapter<AdapterRecicleViewLogs.ViewHolderLogs>{

    ArrayList<Registry> listLogs;

    public AdapterRecicleViewLogs(ArrayList<Registry> listLogs) {
        this.listLogs = listLogs;
    }
    public void setListLogs(ArrayList<Registry> listLogs) {
        this.listLogs = listLogs;
    }

    Resources resources;

    @NonNull
    @Override
    public ViewHolderLogs onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_items_logs,null,false);
        RecyclerView.ViewHolder holder = new ViewHolderLogs(view);
//        return (ViewHolderLogs) holder;
        return new ViewHolderLogs(view);

    }

    @Override
    public void onBindViewHolder(@NonNull AdapterRecicleViewLogs.ViewHolderLogs holder, int position) {

        holder.date.setText(listLogs.get(position).getCurrentTime());
        holder.temperature.setText((String.valueOf(listLogs.get(position).getTemperature())));
        holder.name.setText(listLogs.get(position).getRoom_name());
        holder.image.setImageResource(listLogs.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return listLogs.size();
    }

    public class ViewHolderLogs extends RecyclerView.ViewHolder {

        TextView date;
        TextView temperature;
        TextView name;
        ImageView image;

        public ViewHolderLogs(@NonNull View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.date);
            temperature = (TextView) itemView.findViewById(R.id.temperature);
            name = (TextView) itemView.findViewById(R.id.name);
            image =(ImageView) itemView.findViewById(R.id.image);
        }

//        public void asignarDatos(Registry registry) {
//
//        }
    }
}
