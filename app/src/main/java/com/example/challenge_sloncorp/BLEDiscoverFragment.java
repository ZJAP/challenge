package com.example.challenge_sloncorp;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.challenge_sloncorp.Adapters.RVAdapter;

import java.util.ArrayList;



public class BLEDiscoverFragment extends DialogFragment {

    private ArrayList<BluetoothDevice> DevicesList = new ArrayList<>();
    private android.bluetooth.BluetoothAdapter mBluetoothAdapter;
    static final int REQUEST_ENABLE_LOCATION = 2;
    RVAdapter adapter;
    RecyclerView recyclerView1;
    BluetoothManager bluetoothManager;
    BLEManager scann;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ble_discover_layout, container, false);
        initRecyclerView(view);
        return view;

    }


    private void initRecyclerView(View view) {

        recyclerView1 = view.findViewById(R.id.recycleview1);

        adapter = new RVAdapter(DevicesList);
        recyclerView1.setAdapter(adapter);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
        scann= new BLEManager(getActivity(), adapter, DevicesList);
    }


    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MainActivity.REQUEST_ENABLE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Restart device scanning
                    if (scann != null) {
                        scann   = new BLEManager(getActivity(),adapter,DevicesList );
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }
}

