package com.example.challenge_sloncorp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.challenge_sloncorp.Adapters.RVAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

//import android.support.v4.content.LocalBroadcastManager;

public class BLEManager implements Serializable {

    private ArrayList<BluetoothDevice> listDevices;
    BluetoothManager bluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    BluetoothLeScanner bluetoothLeScanner;
    RVAdapter adapter;
    private Context context;
    private ScanSettings settings;
    private ScanCallback mLeScanCallback2;
    private Activity activity;
    private Handler handler;



    public BLEManager(Activity activity, RVAdapter adapter, ArrayList<BluetoothDevice> listDevices) {

        this.activity= activity;
        this.adapter=adapter;
        this.listDevices=listDevices;
        init();
    }

    public ArrayList<BluetoothDevice> getListDevices() {
        return listDevices;
    }


    public void init() {

        int REQUEST_ENABLE_BT = 1;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            mLeScanCallback2 = new ScanCallback() {
                @Override
                @TargetApi(21)
                public void onScanResult(int callbackType, final ScanResult result) {
                    super.onScanResult(callbackType, result);

                    activity.runOnUiThread(new Runnable() {
                          @Override
                          public void run() {

                              listDevices.add(result.getDevice());
                              // adapter.setDevices(listDevices)




                              Set<BluetoothDevice> hs = new HashSet();
                              hs.addAll(listDevices);
                              listDevices.clear();
                              listDevices.addAll(hs);
                              adapter.notifyDataSetChanged();
                          }
                    });
               }

                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    super.onBatchScanResults(results);
                    for (ScanResult rs : results) {
                        Log.d("test", "-----");
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    super.onScanFailed(errorCode);
                }

            };

        }
        checkStatusOfPeripheralConnectionAndSetNotifications();
    }

    private void checkStatusOfPeripheralConnectionAndSetNotifications() {
        checkStateAndStartScan(mBluetoothAdapter.getState());
    }

    private void checkStateAndStartScan(int state) {
        Timer timer = new Timer();
        switch (state) {
            case BluetoothAdapter.STATE_ON:
                scanDevice(true);
                timer.cancel();
                break;
            case BluetoothAdapter.STATE_OFF:
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("BasicBLEPeripheralDeviceNoConnection"));
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        checkStatusOfPeripheralConnectionAndSetNotifications();
                    }
                }, 1000);
                break;
            default:
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        checkStatusOfPeripheralConnectionAndSetNotifications();
                    }
                }, 1000);
                break;
        }
    }


    protected void scanDevice ( final boolean enable){

        long SCAN_PERIOD = 10000;

        if (enable) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //aqui problema
                bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

                //this code helps to get ready the JL2 w/o any problem
                mBluetoothAdapter.cancelDiscovery();

                bluetoothLeScanner.startScan(null, settings, mLeScanCallback2);
            } else {
                //this code helps to get ready the JL2 w/o any problem
                mBluetoothAdapter.cancelDiscovery();

                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

            if (bluetoothLeScanner != null) {
                bluetoothLeScanner.stopScan(mLeScanCallback2);
            }
        } else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }


    // Device scan callback.
    BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    listDevices.add(device);
                    // adapter.setDevices(listDevices)

                    Set<BluetoothDevice> hs = new HashSet();
                    hs.addAll(listDevices);
                    listDevices.clear();
                    listDevices.addAll(hs);
                    adapter.notifyDataSetChanged();
                }
            });

        }
    };

}

