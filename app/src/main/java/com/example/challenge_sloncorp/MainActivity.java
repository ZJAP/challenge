package com.example.challenge_sloncorp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.challenge_sloncorp.Adapters.AdapterRecicleViewLogs;
import com.example.challenge_sloncorp.Classes.Registry;
import com.example.challenge_sloncorp.Classes.Room;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MainActivity extends AppCompatActivity {

    ArrayList<Registry> listLogs = new ArrayList<>();
    AdapterRecicleViewLogs adapterLogs;
    static final int REQUEST_ENABLE_LOCATION = 2;
    Button btnSaveLogs;
    Button btnGetSensors;
    RecyclerView recyclerLogs;
    Resources resources;
    int colorYellow;
    int colorOrange;
    int colorBlue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resources = getResources();
        colorYellow = resources.getColor(R.color.colorYellow);
        colorOrange = resources.getColor(R.color.colorOrange);
        colorBlue = resources.getColor(R.color.colorBlue);

        checkPermissions();
        initComponents();
        Room room1 = new Room(1, "room 1", 0, 5, 20, 15, R.drawable.bed_room, (TextView) findViewById(R.id.room1));
        Room room2 = new Room(2, "room 2", 0, 8, 25, 10, R.drawable.living_room, (TextView) findViewById(R.id.room2));
        Room room3 = new Room(3, "room 3", 0, 10, 28, 5, R.drawable.kitchen_room, (TextView) findViewById(R.id.room3));
        sensing_temperature(room1);
        sensing_temperature(room2);
        sensing_temperature(room3);
    }


    public void checkPermissions() {

//      LOCATION PERMISSIONS -> FOR BLUETOOTH FUNCTIONALITY

        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_ENABLE_LOCATION);
            } else {
                LocationListener locationListener = new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        // Called when a new location is found by the network location provider.
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }
                };
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            }
        }
//       EXTERNAL ST0RAGE PERMISSIONS
        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, PackageManager.PERMISSION_GRANTED);

    }

    public void initComponents() {
        btnSaveLogs = (Button) findViewById(R.id.btn_save_logs);
        btnSaveLogs.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                createSaveFile();
            }
        });

        btnGetSensors = (Button) findViewById(R.id.btn_get_sensors);
        btnGetSensors.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getSensorsDialog();

            }
        });

        recyclerLogs = (RecyclerView) findViewById(R.id.recyclerLogs);
        recyclerLogs.setLayoutManager(new LinearLayoutManager((this)));
        adapterLogs = new AdapterRecicleViewLogs(listLogs);
        recyclerLogs.setAdapter(adapterLogs);

    }


    private void sensing_temperature(final Room room) {

        final int var_time = room.getVariation_time() * 1000;
        final boolean is_started = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                int minTemp = room.getMin_temperature();
                int maxTemp = room.getMax_temperature();
                while (is_started) {
                    final int random_temperature = new Random().nextInt((maxTemp - minTemp) + 1) + minTemp;
                    room.setCurrent_temperature(random_temperature);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            TextView txt_room = room.getCircleView();
                            txt_room.setText(String.valueOf(random_temperature));
                            if (random_temperature > 4 & random_temperature < 15) {
                                ((GradientDrawable) txt_room.getBackground()).setColor(colorBlue);
                            } else if (random_temperature >= 15 & random_temperature < 25) {
                                ((GradientDrawable) txt_room.getBackground()).setColor(colorYellow);
                            } else {
                                ((GradientDrawable) txt_room.getBackground()).setColor(colorOrange);
                            }
                        }
                    });

                    if (random_temperature < 12 & listLogs.size() < 30) {
                        createLog(room);
                    }

                    try {
                        Thread.sleep(var_time);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void createLog(Room room) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());
        final Registry registry = new Registry(currentDateandTime, room.getCurrent_temperature(), room.getName(), room.getImage());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listLogs.add(registry);
                adapterLogs.notifyDataSetChanged();
            }
        });
    }


    public void createSaveFile() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, "logs.txt");
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {

                try {
                    Uri uri = data.getData();
                    OutputStream outputStream = getContentResolver().openOutputStream(uri);
                    String logs_file = new String("Date" + "," + "Temperature" + "," + "Room" + "\n");
                    for (Registry reg : listLogs) {
                        logs_file = logs_file + reg.getCurrentTime() + "," + reg.getTemperature() + "," +reg.getRoom_name() + "\n";
                    }
                    outputStream.write(logs_file.getBytes());
                    outputStream.close();
                    Toast.makeText(this, "File saved successfully", Toast.LENGTH_SHORT).show();

                    Intent intentShare = new Intent(Intent.ACTION_SEND);
                    intentShare.setType("application/txt");
                    intentShare.putExtra(Intent.EXTRA_STREAM, Uri.parse(String.valueOf(uri)));

                    startActivity(Intent.createChooser(intentShare, "Share the file ..."));


                } catch (IOException e) {
                    Toast.makeText(this, "Failed to save file", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "File not saved", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getSensorsDialog() {
        BLEDiscoverFragment dialog = new BLEDiscoverFragment();
        dialog.show(getSupportFragmentManager(), "GET SENSORS");
    }

}






