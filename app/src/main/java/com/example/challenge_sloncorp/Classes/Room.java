package com.example.challenge_sloncorp.Classes;

import android.media.Image;
import android.widget.TextView;

public class Room {
    private Integer id;
    private String name;
    private Integer current_temperature;
    private Integer min_temperature;
    private Integer max_temperature;
    private Integer variation_time;
    private Integer image;
    private TextView circleView;


    public Room(Integer id, String name, Integer current_temperature, Integer min_temperature, Integer max_temperature, Integer variation_time, Integer image, TextView circleView) {
        this.id = id;
        this.name = name;
        this.current_temperature = current_temperature;
        this.min_temperature = min_temperature;
        this.max_temperature = max_temperature;
        this.variation_time = variation_time;
        this.image = image;
        this.circleView = circleView;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCurrent_temperature() {
        return current_temperature;
    }

    public void setCurrent_temperature(Integer current_temperature) {
        this.current_temperature = current_temperature;
    }

    public Integer getMin_temperature() {
        return min_temperature;
    }

    public void setMin_temperature(Integer min_temperature) {
        this.min_temperature = min_temperature;
    }

    public Integer getMax_temperature() {
        return max_temperature;
    }

    public void setMax_temperature(Integer max_temperature) {
        this.max_temperature = max_temperature;
    }

    public Integer getVariation_time() {
        return variation_time;
    }

    public void setVariation_time(Integer variation_time) {
        this.variation_time = variation_time;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public TextView getCircleView() {
        return circleView;
    }

    public void setCircleView(TextView circleView) {
        this.circleView = circleView;
    }

}
