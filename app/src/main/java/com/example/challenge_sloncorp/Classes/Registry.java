package com.example.challenge_sloncorp.Classes;

import android.media.Image;

import java.io.Serializable;
import java.util.Date;

public class Registry implements Serializable {

    private String currentTime;
    private Integer temperature;
    private String room_name;
    private Integer image;

    public Registry(String currentTime, Integer temperature, String room_name, Integer image) {
        this.currentTime = currentTime;
        this.temperature = temperature;
        this.room_name = room_name;
        this.image = image;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }


    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

}
